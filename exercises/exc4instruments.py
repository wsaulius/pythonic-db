import mysql.connector
import logging

user = 'root'
password = '1My_sql!SQL'

db = mysql.connector.connect(host="localhost",
                             user= user,
                             password= password,
                             database="music")
print (db)

logger = logging.getLogger('errorlog')
hdlr = logging.FileHandler('errors.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)

create_tables = """
CREATE TABLE IF NOT EXISTS instruments (
    instrument_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    name VARCHAR(30) NOT NULL,
    family VARCHAR(30) NOT NULL,
    difficulty ENUM('easy', 'medium', 'hard') NOT NULL
);
"""

def insert_instruments(connection, instrument_values):
    if not instrument_values:
        return

    print ("tupple data: %s" % instrument_values)
    insert_sql = """INSERT INTO instruments (name, family, difficulty) VALUES (%s, %s, %s)"""
    connection.cursor().executemany(insert_sql, instrument_values)
    connection.commit()

def get_instruments_count(connection):
    get_count_sql = """SELECT family, count(*) as count FROM instruments GROUP BY family"""
    cursor = connection.cursor(mysql.connector.connection.MySQLCursorDict)
    cursor.execute(get_count_sql)
    return cursor.fetchall()

def get_instruments_cnt(connection):
    get_count_sql = """SELECT family, count(*) as count FROM instruments GROUP BY family"""
    cursor = connection.cursor(mysql.connector.connection.MySQLCursorDict)
    cursor.execute(get_count_sql)
    return cursor.fetchall()

try:

    cursor = db.cursor()
    cursor.execute(create_tables)
    db.commit()

    instruments = [
        ('guitar', 'strings', 'medium'),
        ('piano', 'keyboard', 'hard'),
        ('harp', 'strings', 'hard' ),
        ('triangle', 'percussion', 'easy'),
        ('flute', 'woodwind', 'medium'),
        ('violin', 'strings', 'medium'),
        ('tambourine', 'percussion', 'easy'),
        ('organ', 'keyboard', 'hard')]

    with db:
        insert_instruments(db, instruments)
        logger.info('DB OK: ' + " added)" )
        print (instruments)

        result = get_instruments_count(db)

        print ("groups: ", result)
        print( "%d instrument GROUPS in DB" % len(result) )

        result = get_instruments_cnt(db)
        print ("nodict: ", result)

#except Exception, e:
#similarly:

except Exception as e:
    logger.error('DB Error: ' + str(e))
