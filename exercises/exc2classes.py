
# ORM (object relational mapping) in use
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer, Date
import logging

logger = logging.getLogger('errorlog')
hdlr = logging.FileHandler('errors-%s.log' % 'cars')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.DEBUG)

eng = create_engine('mysql://root:1My_sql!SQL@localhost:3306/car_rental')
print(eng)

base = declarative_base()

class Cars(base):
    __tablename__ = 'cars'

    car_id = Column(Integer, primary_key=True, autoincrement=True)
    producer = Column(String(30), nullable=False)
    model = Column(String(30), nullable=False)
    year = Column(Integer, nullable=False)
    horse_power = Column(Integer, nullable=False)
    price_per_day = Column(Integer, nullable=False)

    def __repr__(self):
       return '<Car: id=%d :: producer=%s :: model=%s :: year=%d :: horse_power=%d :: price_per_day=%d>' \
              % ( self.car_id, self.producer, self.model, self.year, self.horse_power, self.price_per_day )

class Clients(base):
    __tablename__ = 'clients'

    client_id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(30), nullable=False)
    surname = Column(String(30), nullable=False)
    address = Column(String(30), nullable=False)
    city = Column(String(30), nullable=False)

    def __repr__(self):
       return '<Clients: id=%d name=%s surname=%s, address=%s, city=%s>' \
              % ( self.client_id, self.name, self.surname, self.address, self.city )

class Bookings(base):
    __tablename__ = 'bookings'

    booking_id = Column(Integer, primary_key=True, autoincrement=True)
    client_id = Column(Integer, nullable=False)
    car_id = Column(Integer, nullable=False)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=False)
    total_amount = Column(Integer, nullable=False)

    def __repr__(self):
        return "<Booking: id={self.booking_id}, client_id={self.client_id}, car_id={self.car_id}, " \
               "start_date={self.start_date}, end_date={self.end_date}, total_amount={self.total_amount}>"

try:

    base.metadata.create_all(eng)
    print (base.metadata)
    #help(base.metadata)

    from sqlalchemy.orm import sessionmaker
    #help(sessionmaker)

    Session = sessionmaker(bind=eng, autoflush = True, autocommit = False, expire_on_commit = True)
    session = Session()

    # Table as OBJECT! (ORM beauty)
    client_1 = Clients(name='Jan', surname='Kowalski', address='ul. Florianska 12', city='Krakow')
    session.add(client_1)

    car_1 = Cars(producer='Seat', model='Leon', year=2016, horse_power=80, price_per_day=200)
    session.add(car_1)
    print (session)

    for client in session.query(Clients).all():
        print(client)

    for car in session.query(Cars).all():
        print(car)

    session.commit()

except Exception as e:

   # Rollback in case there is any error
   print (str(e))

   logger.error('Error occurred: ' + str(e))
   session.rollback()
