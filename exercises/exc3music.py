
import mysql.connector

user = 'root'
password = '1My_sql!SQL'

db = mysql.connector.connect(host="localhost",
                             user= user,
                             password= password )
print (db)

import logging

print_db = "CREATE DATABASE IF NOT EXISTS %s"
create_database = print_db % 'music'

create_tables = """
CREATE TABLE IF NOT EXISTS instruments (
    instrument_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    name VARCHAR(30) NOT NULL,
    family VARCHAR(30) NOT NULL,
    difficulty ENUM('easy', 'medium', 'hard') NOT NULL
);
"""
use_database = "USE music"

logger = logging.getLogger('errorlog')
hdlr = logging.FileHandler('errors-%s.log' % 'music')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.DEBUG)

try:
    cursor = db.cursor()
    cursor.execute(create_database)

    cursor.execute(use_database)
    print ("DB select OK")

    cursor.execute(create_tables)
    print ("DB created OK")

    #db.commit()
    logger.info('DB OK: ' + create_database + " executed.")

#except Exception, e:
#similarly:

except Exception as e:

    logger.error('DB Error: ' + str(e))

    #db.commit()
    #cursor.execute(create_tables)