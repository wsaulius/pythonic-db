
import sqlalchemy
import logging
import mysql.connector

from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer, Date, ForeignKey, MetaData

user = 'root'
password = '1My_sql!SQL'

db = mysql.connector.connect(host="localhost",
                             user= user,
                             password= password,
                             database= "cinematic" )

directors = [ ( 'Darabont', 'Frank', 7),
              ( 'Coppola', 'Francis Ford', 8),
              ( 'Tarantino', 'Quentin', 10),
              ( 'Nolan', 'Christopher', 9),
              ( 'Fincher', 'David', 7) ]

shitf_ids = 0
movies = [ ('The Shawshank Redemption', 1994, 'Drama', shitf_ids+1, 8),
            ('The Green Mile', 1999, 'Drama', shitf_ids+1, 6),
            ('The Godfather', 1972, 'Crime', shitf_ids+2, 7),
            ('The Godfather III', 1990, 'Crime', shitf_ids+2, 6),
            ('Pulp Fiction', 1994, 'Crime', shitf_ids+3, 9),
            ('Inglourious Basterds', 2009, 'War', shitf_ids+3, 8),
            ('The Dark Knight', 2008, 'Action', shitf_ids+4, 9),
            ('Interstellar', 2014, 'Sci-fi', shitf_ids+4, 8),
            ('The Prestige', 2006, 'Drama', shitf_ids+4, 10),
            ('Fight Club', 1999, 'Drama', shitf_ids+5, 7),
            ('Zodiac', 2007, 'Crime', shitf_ids+5, 5),
            ('Seven', 1995, 'Drama', shitf_ids+5, 8),
            ('Alien 3', 1992, 'Horror', shitf_ids+5, 5)]

logger = logging.getLogger('errorlog')
hdlr = logging.FileHandler('errors.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)

#
# CREATE TABLE IF NOT EXISTS directors (
#     director_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
#     name VARCHAR(30) NOT NULL,
#     surname VARCHAR(30) NOT NULL,
#     rating INT
#     );

# If you choose to switch the RDBMS implementation, just replace one line!
#eng = create_engine('oracle://root:1My_sql!SQL@localhost:9932/car_rental', echo='debug' )
eng = create_engine('mysql+pymysql://root:1My_sql!SQL@localhost:3306/cinematic')

base = declarative_base()
base.metadata.create_all(eng)
Session = sqlalchemy.orm.sessionmaker(bind=eng)
session = Session()

# Define as a collection of statements.
create_db_tables = [
    "DROP TABLE IF EXISTS movies",
    "DROP TABLE IF EXISTS directors",
    """CREATE TABLE IF NOT EXISTS directors (
        director_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
        name VARCHAR(30) NOT NULL,
        surname VARCHAR(30) NOT NULL,
        rating INT
        )""",
    """CREATE UNIQUE INDEX unique_name_surname ON directors(name,surname)""",
    """CREATE TABLE IF NOT EXISTS movies (
    movie_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    title VARCHAR(30), 
    year INT, 
    category VARCHAR(10), 
    director_id INT, 
    rating INT
    )""",
    """ALTER TABLE movies ADD CONSTRAINT FK_movies_directors 
    FOREIGN KEY (director_id) REFERENCES directors(director_id)"""
    ]

def truncate_db(engine):
    # delete all table data (but keep tables)
    # we do cleanup before test 'cause if previous test errored,
    # DB can contain dust
    meta = MetaData(bind=engine)
    con = engine.connect()
    trans = con.begin()

    # Initialize DB in all statements
    cursor = db.cursor()

    # Note 'multi=True' when calling cursor.execute()
    for result in cursor.execute(';'.join(create_db_tables), multi=True):
        print (result)

    meta.reflect(bind=engine)
    for table in meta.sorted_tables:
        con.execute(f'ALTER TABLE {table.name} DISABLE KEYS;')
        print (f'TRUNC DB TABLE {table.name}')
        con.execute(table.delete())
        con.execute(f'ALTER TABLE {table.name} ENABLE KEYS;')
    trans.commit()

def insert_directors(connection, directors):
    if not directors:
        return
    insert_sql = """
    INSERT INTO directors (surname,name,rating) VALUES(%s, %s, %s)"""
    connection.cursor().executemany(insert_sql, directors)

    print('DB OK: ' + str(directors) + " executed.")
    connection.commit()

def insert_movies(connection, movies):
    if not movies:
        return

    insert_sql = """
    INSERT INTO movies (title, year, category, director_id, rating)
    VALUES(%s, %s, %s, %s, %s)"""
    connection.cursor().executemany(insert_sql, movies)
    connection.commit()

try:

    with session:

        truncate_db( eng )
        logger.info('DB DIRECTORS Re-CREATE: ' + str(directors) + " executed.")

        insert_directors(db, directors)
        logger.info('DB OK: ' + str(directors) + " added)" )
        print (directors)

    cursor = db.cursor()
    cursor.execute("SELECT COUNT(*) FROM directors;")
    counter = cursor.fetchone()
    db.commit()
    print ("DB tables OK, inserted %d" % counter)

    with session:
        insert_movies(db, movies)
        logger.info('DB OK: ' + str(movies) + " added)")
        print (movies)

    cursor = db.cursor()
    cursor.execute("SELECT COUNT(*) FROM movies;")
    counter = cursor.fetchone()

    print ("DB tables OK, inserted %d" % counter)

except Exception as e:

    logger.error('DB Error: ' + str(e))
