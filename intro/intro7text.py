
import sqlalchemy
import logging
import mysql.connector

from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from sqlalchemy.orm import relationship
from sqlalchemy.sql import select
from sqlalchemy import join
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer, Date, ForeignKey, MetaData

user = 'root'
password = '1My_sql!SQL'

db = mysql.connector.connect(host="localhost",
                             user= user,
                             password= password,
                             database= "cinematic" )

directors = [ ( 'Darabont', 'Frank', 7),
              ( 'Coppola', 'Francis Ford', 8),
              ( 'Tarantino', 'Quentin', 10),
              ( 'Nolan', 'Christopher', 9),
              ( 'Fincher', 'David', 7) ]

shitf_ids = 0
movies = [ ('The Shawshank Redemption', 1994, 'Drama', shitf_ids+1, 8),
            ('The Green Mile', 1999, 'Drama', shitf_ids+1, 6),
            ('The Godfather', 1972, 'Crime', shitf_ids+2, 7),
            ('The Godfather III', 1990, 'Crime', shitf_ids+2, 6),
            ('Pulp Fiction', 1994, 'Crime', shitf_ids+3, 9),
            ('Inglourious Basterds', 2009, 'War', shitf_ids+3, 8),
            ('The Dark Knight', 2008, 'Action', shitf_ids+4, 9),
            ('Interstellar', 2014, 'Sci-fi', shitf_ids+4, 8),
            ('The Prestige', 2006, 'Drama', shitf_ids+4, 10),
            ('Fight Club', 1999, 'Drama', shitf_ids+5, 7),
            ('Zodiac', 2007, 'Crime', shitf_ids+5, 5),
            ('Seven', 1995, 'Drama', shitf_ids+5, 8),
            ('Alien 3', 1992, 'Horror', shitf_ids+5, 5)]

logger = logging.getLogger('errorlog')
hdlr = logging.FileHandler('errors.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)

#
# CREATE TABLE IF NOT EXISTS directors (
#     director_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
#     name VARCHAR(30) NOT NULL,
#     surname VARCHAR(30) NOT NULL,
#     rating INT
#     );

# If you choose to switch the RDBMS implementation, just replace one line!
#eng = create_engine('oracle://root:1My_sql!SQL@localhost:9932/car_rental', echo='debug' )
eng = create_engine('mysql+pymysql://root:1My_sql!SQL@localhost:3306/cinematic')

base = declarative_base()
base.metadata.create_all(eng)
Session = sqlalchemy.orm.sessionmaker(bind=eng)
session = Session()

# Define as a collection of statements.
create_db_tables = [
    "DROP TABLE IF EXISTS movies",
    "DROP TABLE IF EXISTS directors",
    """CREATE TABLE IF NOT EXISTS directors (
        director_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
        name VARCHAR(30) NOT NULL,
        surname VARCHAR(30) NOT NULL,
        rating INT
        )""",
    """CREATE TABLE IF NOT EXISTS movies (
    movie_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    title VARCHAR(30), 
    year INT, 
    category VARCHAR(10), 
    director_id INT, 
    rating INT,
      FOREIGN KEY (director_id) REFERENCES directors(director_id)
    )"""]

def truncate_db(engine):
    # delete all table data (but keep tables)
    # we do cleanup before test 'cause if previous test errored,
    # DB can contain dust
    meta = MetaData(bind=engine)
    con = engine.connect()
    trans = con.begin()

    # Initialize DB in all statements
    cursor = db.cursor()

    # Note 'multi=True' when calling cursor.execute()
    for result in cursor.execute(';'.join(create_db_tables), multi=True):
        print (result)

    meta.reflect(bind=engine)
    for table in meta.sorted_tables:
        con.execute(f'ALTER TABLE {table.name} DISABLE KEYS;')
        print (f'TRUNC DB TABLE {table.name}')
        con.execute(table.delete())
        con.execute(f'ALTER TABLE {table.name} ENABLE KEYS;')
    trans.commit()

def insert_directors(connection, directors):
    if not directors:
        return
    insert_sql = """
    INSERT INTO directors (surname,name,rating) VALUES(%s, %s, %s)"""
    connection.cursor().executemany(insert_sql, directors)

    print('DB OK: ' + str(directors) + " executed.")

    connection.commit()

def insert_movies(connection, movies):
    if not movies:
        return

    insert_sql = """
    INSERT INTO movies (title, year, category, director_id, rating)
    VALUES(%s, %s, %s, %s, %s)"""
    connection.cursor().executemany(insert_sql, movies)
    connection.commit()

from sqlalchemy.sql import select

base = declarative_base()

class Directors(base):
    __tablename__ = 'directors'

    director_id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(30), nullable=False)
    surname = Column(String(30), nullable=False)
    rating = Column(Integer, nullable=False)
    movies = relationship('Movies', back_populates='directors', cascade="all, delete", passive_deletes=True)

    def __repr__(self):
       return f"<Directors: director_id=%d :: name=%s :: surname=%s :: rating=%d>" \
              % ( self.director_id, self.name, self.surname, self.rating )

class Movies(base):
    __tablename__ = 'movies'

    movie_id = Column(Integer, primary_key=True, autoincrement=True)
    director_id = Column(Integer, ForeignKey('directors.director_id', ondelete="CASCADE"), nullable=False)
    title = Column(String(30), nullable=False)
    year = Column(Integer, nullable=False)
    category = Column(String(10), nullable=False)
    rating = Column(Integer, nullable=False)
    directors = relationship('Directors', back_populates='movies')

    def __repr__(self):
       return f"<Movies: movie_id=%d :: title=%s :: year=%d :: category=%s :: director_id=%d :: rating=%d>" \
              % ( self.movie_id, self.title, self.year, self.category, self.director_id, self.rating )

try:

    with session:

        truncate_db( eng )
        logger.info('DB DIRECTORS Re-CREATE: ' + str(directors) + " executed.")

        insert_directors(db, directors)
        logger.info('DB OK: ' + " added)" )
        print (directors)

    cursor = db.cursor()
    cursor.execute("SELECT COUNT(*) FROM directors;")
    counter = cursor.fetchone()
    db.commit()
    print ("DB tables OK, inserted %d" % counter)

    with session:
        insert_movies(db, movies)
        logger.info('DB OK: ' + " added)")
        print (movies)

    cursor = db.cursor()
    cursor.execute("SELECT COUNT(*) FROM movies;")
    counter = cursor.fetchone()

    print ("DB tables OK, inserted %d" % counter)

    conn = eng.connect()

    # Basically, we want to execute the following SQL JOIN
    # SELECT d.director_id, d.name, d.surname, d.rating
    # FROM movies m, directors d WHERE m.director_id = d.director_id AND m.category = 'Drama'

    j = join(Movies, Directors, Movies.director_id == Directors.director_id)
    s = select([Directors]).select_from(j).where(Movies.category == 'Drama' )
    result = conn.execute(s)

    print ( result )
    for director in result:
        print( director )

    # What about JOIN?
    print (j);

    ##### TASK6 #####
    #
    # List the titles of movies from the Crime category that were made after 1994.
    # Use select () and query ().
    #

    print('''  ##### TASK6 ##### ''');
    from sqlalchemy import and_
    s = select([Movies]).where(Movies.category == 'Crime' ).filter( and_(Movies.year >= 1994) )

    result = conn.execute(s).fetchall()
    print(result)

    ##### TASK7 #####
    #
    # List the categories of all movies and their rankings, for the films that were
    # produced between 2000 and 2010 and had rankings greater than 7, also, sort them
    # by their ranking. Use select () and query ().
    #
    # SELECT movies.category, movies.rating FROM movies where movies.year > 2000
    # AND movies.year < 2010 AND rating > 7 ORDER BY rating DESC
    #

    print ('''  ##### TASK7 ##### ''');
    from sqlalchemy import desc

    s = select([Movies]).where(Movies.year >= 2000,Movies.year <= 2010 )\
        .order_by(desc(Movies.rating))\
        .filter( and_(Movies.rating >= 7) )

    result = conn.execute(s).fetchall()
    print(result)

    ##### TASK8 #####
    #
    # List the names of all directors whose ranking is greater than or equal to 6 and whose first
    # name starts with 'D' or ends with 'n'. Use select () and query ()
    #

    print ('''  ##### TASK8 ##### ''');
    from sqlalchemy import text
    from sqlalchemy import or_
    from sqlalchemy import func

    string = "sample string"
    print ( string.startswith( "s" ) == True )
    print ( string.split( " " )[0].endswith("e") == True)
    print(string.split(" ")[1].endswith("g") == True)

    s = select([Directors]).where(Directors.rating >= 6 )\
        .filter( or_( func.lower( Directors.name ).startswith( 'd' ),
                      Directors.name.endswith( 'n' )
                    )
               )

    result = conn.execute(s).fetchall()
    print(result)

    for rating in range (8,10):

        print ('''  ##### SAME AS ##### for rating %d''' % rating );
        s = select([Directors]).filter( and_( (Directors.rating >= rating ),
                          or_( func.lower( Directors.name ).startswith( 'd' ),
                          Directors.name.endswith( 'n' ) )
                        )
                   )

        result = conn.execute(s).fetchall()
        print(result)

except Exception as e:

    logger.error('DB Error: ' + str(e))

